package absensi.modul.mapper;

import absensi.modul.model.Absensi;
import absensi.modul.model.AbsensiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AbsensiMapper {
    long countByExample(AbsensiExample example);

    int deleteByExample(AbsensiExample example);

    int deleteByPrimaryKey(Integer idAbsensi);

    int insert(Absensi record);

    int insertSelective(Absensi record);

    List<Absensi> selectByExample(AbsensiExample example);

    Absensi selectByPrimaryKey(Integer idAbsensi);

    int updateByExampleSelective(@Param("record") Absensi record, @Param("example") AbsensiExample example);

    int updateByExample(@Param("record") Absensi record, @Param("example") AbsensiExample example);

    int updateByPrimaryKeySelective(Absensi record);

    int updateByPrimaryKey(Absensi record);
}