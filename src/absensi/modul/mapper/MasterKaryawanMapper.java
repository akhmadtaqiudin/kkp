package absensi.modul.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import absensi.modul.model.MasterKaryawan;
import absensi.modul.model.MasterKaryawanExample;

public interface MasterKaryawanMapper {
    long countByExample(MasterKaryawanExample example);

    int deleteByExample(MasterKaryawanExample example);

    int deleteByPrimaryKey(String nikKaryawan);

    int insert(MasterKaryawan record);

    int insertSelective(MasterKaryawan record);

    List<MasterKaryawan> selectByExample(MasterKaryawanExample example);

    MasterKaryawan selectByPrimaryKey(String nikKaryawan);
    
    String selectMaxId(MasterKaryawan record);

    int updateByExampleSelective(@Param("record") MasterKaryawan record, @Param("example") MasterKaryawanExample example);

    int updateByExample(@Param("record") MasterKaryawan record, @Param("example") MasterKaryawanExample example);

    int updateByPrimaryKeySelective(MasterKaryawan record);

    int updateByPrimaryKey(MasterKaryawan record);
}