package absensi.modul.mapper;

import absensi.modul.model.Bonus;
import absensi.modul.model.BonusExample;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BonusMapper {
    long countByExample(BonusExample example);

    int deleteByExample(BonusExample example);

    int deleteByPrimaryKey(Integer idBonus);

    int insert(Bonus record);

    int insertSelective(Bonus record);

    List<Bonus> selectByExample(BonusExample example);
    
    List<Bonus> selectAll(Bonus record);

    Bonus selectByPrimaryKey(Integer idBonus);

    int updateByExampleSelective(@Param("record") Bonus record, @Param("example") BonusExample example);

    int updateByExample(@Param("record") Bonus record, @Param("example") BonusExample example);

    int updateByPrimaryKeySelective(Bonus record);

    int updateByPrimaryKey(Bonus record);
}