package absensi.modul.mapper;

import absensi.modul.model.MasterJabatan;
import absensi.modul.model.MasterJabatanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MasterJabatanMapper {
    long countByExample(MasterJabatanExample example);

    int deleteByExample(MasterJabatanExample example);

    int deleteByPrimaryKey(Integer idJabatan);

    int insert(MasterJabatan record);

    int insertSelective(MasterJabatan record);

    List<MasterJabatan> selectByExample(MasterJabatanExample example);

    MasterJabatan selectByPrimaryKey(Integer idJabatan);
    
    MasterJabatan selectByName(MasterJabatan record);

    int updateByExampleSelective(@Param("record") MasterJabatan record, @Param("example") MasterJabatanExample example);

    int updateByExample(@Param("record") MasterJabatan record, @Param("example") MasterJabatanExample example);

    int updateByPrimaryKeySelective(MasterJabatan record);

    int updateByPrimaryKey(MasterJabatan record);
}