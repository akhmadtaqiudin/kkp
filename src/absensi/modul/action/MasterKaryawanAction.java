package absensi.modul.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import absensi.core.action.CoreAction;
import absensi.modul.mapper.MasterKaryawanMapper;
import absensi.modul.model.MasterKaryawan;
import absensi.modul.model.MasterKaryawanExample;

public class MasterKaryawanAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strTanggal;
	private MasterKaryawan karyawan;
	SimpleDateFormat sim = new SimpleDateFormat("dd/MM/yyyy");
	private List<MasterKaryawan> listKaryawan = new ArrayList<>();
	private MasterKaryawanMapper masterKaryawanMapper = (MasterKaryawanMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("masterKaryawanMapper");
	
	public String searchAllKaryawan(){
		System.out.println("jalankan method searchAllKaryawan");
		
		if(karyawan == null){
			karyawan = new MasterKaryawan();
			karyawan.setNamaKaryawan("");
		}
		
		MasterKaryawanExample examp = new MasterKaryawanExample();
		examp.createCriteria().andNamaKaryawanLike("%"+karyawan.getNamaKaryawan()+"%");
		listKaryawan = masterKaryawanMapper.selectByExample(examp);
		
		return SUCCESS;
	}
	
	public String searchToList(){
		
		MasterKaryawanExample examp = new MasterKaryawanExample();
		listKaryawan = masterKaryawanMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("jalankan method searchById");
		
		karyawan = masterKaryawanMapper.selectByPrimaryKey(karyawan.getNikKaryawan());
		setStrTanggal(sim.format(karyawan.getTanggalLahir()));
		return SUCCESS;
	}
	
	public String saveKaryawan(){
		System.out.println("jalankan method saveKaryawan");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
		String d[] = sdf.format(new Date()).split("/"),kode="";
		kode+=d[0]+d[1];
		
		String no = "";
		no = masterKaryawanMapper.selectMaxId(karyawan);
		if(no==null){
			no = "0";
		}else{
			no = no.substring(6);
		}
		
		int x = Integer.parseInt(no);
		x++;
		if(x <99 && x>=10){
			kode+="00"+x;
		}else if(x>99 && x<1000){
			kode+="0"+x;
		}else if(x>=999){
			kode+=x;
		}else{
			kode+="000"+x;
		}
		
		try {
			karyawan.setTanggalLahir(sim.parse(getStrTanggal()));
			karyawan.setNikKaryawan(kode);
			masterKaryawanMapper.insert(karyawan);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String editKaryawan(){
		System.out.println("jalankan method editKaryawan");
		
		masterKaryawanMapper.updateByPrimaryKey(karyawan);
		return SUCCESS;
	}
	
	public String deleteKaryawan(){
		System.out.println("jalankan method deleteKaryawan");
		
		masterKaryawanMapper.deleteByPrimaryKey(karyawan.getNikKaryawan());
		return SUCCESS;
	}
	
	public MasterKaryawan getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(MasterKaryawan karyawan) {
		this.karyawan = karyawan;
	}
	public List<MasterKaryawan> getListKaryawan() {
		return listKaryawan;
	}
	public void setListKaryawan(List<MasterKaryawan> listKaryawan) {
		this.listKaryawan = listKaryawan;
	}
	public MasterKaryawanMapper getMasterKaryawanMapper() {
		return masterKaryawanMapper;
	}
	public void setMasterKaryawanMapper(MasterKaryawanMapper masterKaryawanMapper) {
		this.masterKaryawanMapper = masterKaryawanMapper;
	}
	public String getStrTanggal() {
		return strTanggal;
	}
	public void setStrTanggal(String strTanggal) {
		this.strTanggal = strTanggal;
	}

	public SimpleDateFormat getSim() {
		return sim;
	}

	public void setSim(SimpleDateFormat sim) {
		this.sim = sim;
	}
}
