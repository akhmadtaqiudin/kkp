package absensi.modul.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import absensi.core.action.CoreAction;
import absensi.modul.mapper.BonusMapper;
import absensi.modul.model.Bonus;

public class BonusAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private Bonus bonus;
	private List<Bonus> listBonus = new ArrayList<>();
	private BonusMapper bonusMapper = (BonusMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("bonusMapper");
	
	public String searchAllBonus(){
		System.out.println("Jalankan method searchAllBonus");
		
		if(bonus == null){
			bonus = new Bonus();
			bonus.setNamaKaryawan("");
		}
		
		listBonus = bonusMapper.selectAll(bonus);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method searchById");
		bonus = bonusMapper.selectByPrimaryKey(bonus.getIdBonus());
		return SUCCESS;
	}
	
	public String saveBonus(){
		System.out.println("Jalankan method saveBonus");
		bonusMapper.insert(bonus);
		return SUCCESS;
	}
	
	public String editBonus(){
		System.out.println("Jalankan method editBonus");
		bonusMapper.updateByPrimaryKey(bonus);
		return SUCCESS;
	}
	
	public String deleteBonus(){
		System.out.println("Jalankan method deleteBonus");
		bonusMapper.deleteByPrimaryKey(bonus.getIdBonus());
		return SUCCESS;
	}
	public Bonus getBonus() {
		return bonus;
	}
	public void setBonus(Bonus bonus) {
		this.bonus = bonus;
	}
	public List<Bonus> getListBonus() {
		return listBonus;
	}
	public void setListBonus(List<Bonus> listBonus) {
		this.listBonus = listBonus;
	}
	public BonusMapper getBonusMapper() {
		return bonusMapper;
	}
	public void setBonusMapper(BonusMapper bonusMapper) {
		this.bonusMapper = bonusMapper;
	}
}
