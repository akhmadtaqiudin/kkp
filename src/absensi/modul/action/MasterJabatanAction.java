package absensi.modul.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import absensi.core.action.CoreAction;
import absensi.modul.mapper.MasterJabatanMapper;
import absensi.modul.model.MasterJabatan;
import absensi.modul.model.MasterJabatanExample;

public class MasterJabatanAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private MasterJabatan jabatan;
	private List<MasterJabatan> listJabatan = new ArrayList<>();
	private MasterJabatanMapper masterJabatanMapper = (MasterJabatanMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("masterJabatanMapper");
	
	public String searchAllJabatan(){
		System.out.println("jalankan method searchAllJabatan");
		
		if(jabatan == null){
			jabatan = new MasterJabatan();
			jabatan.setNamaJabatan("");
		}
		
		MasterJabatanExample examp = new MasterJabatanExample();
		examp.createCriteria().andNamaJabatanLike("%"+jabatan.getNamaJabatan()+"%");
		listJabatan = masterJabatanMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchToList(){
		System.out.println("jalankan method searchToList");
		
		MasterJabatanExample examp = new MasterJabatanExample();
		listJabatan = masterJabatanMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchByName(){
		System.out.println("jalankan method searchByName");
		
		jabatan = masterJabatanMapper.selectByName(jabatan);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("jalankan method searchById");
		
		jabatan = masterJabatanMapper.selectByPrimaryKey(jabatan.getIdJabatan());
		return SUCCESS;
	}
	
	public String saveJabatan(){
		System.out.println("jalankan method saveJabatan");
		
		masterJabatanMapper.insert(jabatan);
		return SUCCESS;
	}
	
	public String editJabatan(){
		System.out.println("jalankan method editJabatan");
		
		masterJabatanMapper.updateByPrimaryKey(jabatan);
		return SUCCESS;
	}
	
	public String deleteJabatan(){
		System.out.println("jalankan method deleteJabatan");
		
		masterJabatanMapper.deleteByPrimaryKey(jabatan.getIdJabatan());
		return SUCCESS;
	}
	public MasterJabatan getJabatan() {
		return jabatan;
	}
	public void setJabatan(MasterJabatan jabatan) {
		this.jabatan = jabatan;
	}
	public List<MasterJabatan> getListJabatan() {
		return listJabatan;
	}
	public void setListJabatan(List<MasterJabatan> listJabatan) {
		this.listJabatan = listJabatan;
	}
	public MasterJabatanMapper getMasterJabatanMapper() {
		return masterJabatanMapper;
	}
	public void setMasterJabatanMapper(MasterJabatanMapper masterJabatanMapper) {
		this.masterJabatanMapper = masterJabatanMapper;
	}
}
