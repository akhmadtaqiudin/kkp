package absensi.modul.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import absensi.core.action.CoreAction;
import absensi.modul.mapper.AbsensiMapper;
import absensi.modul.model.Absensi;
import absensi.modul.model.AbsensiExample;

public class AbsensiAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strDateAwal;
	private String strDateAkhir;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private Absensi absensi;
	private List<Absensi> listAbsensi = new ArrayList<>();
	private AbsensiMapper absensiMapper = (AbsensiMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("absensiMapper");
	
	public String searchAllAbsensi(){
		System.out.println("jalankan method searchAllAbsensi");
		
		try {
			AbsensiExample examp = new AbsensiExample();
			if(strDateAwal == null || strDateAwal.equalsIgnoreCase("") && strDateAkhir == null || strDateAkhir.equalsIgnoreCase("")){
				examp.createCriteria().andTanggalAbsensiBetween(new Date(), new Date());
			}else{			
			examp.createCriteria().andTanggalAbsensiBetween(sdf.parse(getStrDateAwal()), sdf.parse(getStrDateAkhir()));
			listAbsensi = absensiMapper.selectByExample(examp);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String insertBySystem(){
		System.out.println("jalankan method absenKaryawan");
		
		try {
			absensi.setHadir(1);
			absensi.setTanggalAbsensi(new Date());
			absensiMapper.insert(absensi);
		} catch (Exception e) {
			//addActionError("Data dengan NIK "+absensi.getNik()+" pada Tanggal "+absensi.getTanggal()+" sudah terdaftar pada database");
			return ERROR;
		}	
		addActionMessage("Absen berhasil, Selamat bekerja");
		return SUCCESS;
	}
	
	public String absenAdmin(){
		System.out.println("jalankan method absenAdmin");
		
		absensiMapper.insert(absensi);
		return SUCCESS;
	}
	public Absensi getAbsensi() {
		return absensi;
	}
	public void setAbsensi(Absensi absensi) {
		this.absensi = absensi;
	}
	public List<Absensi> getListAbsensi() {
		return listAbsensi;
	}
	public void setListAbsensi(List<Absensi> listAbsensi) {
		this.listAbsensi = listAbsensi;
	}
	public AbsensiMapper getAbsensiMapper() {
		return absensiMapper;
	}
	public void setAbsensiMapper(AbsensiMapper absensiMapper) {
		this.absensiMapper = absensiMapper;
	}
	public String getStrDateAwal() {
		return strDateAwal;
	}
	public void setStrDateAwal(String strDateAwal) {
		this.strDateAwal = strDateAwal;
	}
	public String getStrDateAkhir() {
		return strDateAkhir;
	}
	public void setStrDateAkhir(String strDateAkhir) {
		this.strDateAkhir = strDateAkhir;
	}
}
