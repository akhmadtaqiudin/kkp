package absensi.modul.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BonusExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BonusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdBonusIsNull() {
            addCriterion("id_bonus is null");
            return (Criteria) this;
        }

        public Criteria andIdBonusIsNotNull() {
            addCriterion("id_bonus is not null");
            return (Criteria) this;
        }

        public Criteria andIdBonusEqualTo(Integer value) {
            addCriterion("id_bonus =", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusNotEqualTo(Integer value) {
            addCriterion("id_bonus <>", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusGreaterThan(Integer value) {
            addCriterion("id_bonus >", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_bonus >=", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusLessThan(Integer value) {
            addCriterion("id_bonus <", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusLessThanOrEqualTo(Integer value) {
            addCriterion("id_bonus <=", value, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusIn(List<Integer> values) {
            addCriterion("id_bonus in", values, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusNotIn(List<Integer> values) {
            addCriterion("id_bonus not in", values, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusBetween(Integer value1, Integer value2) {
            addCriterion("id_bonus between", value1, value2, "idBonus");
            return (Criteria) this;
        }

        public Criteria andIdBonusNotBetween(Integer value1, Integer value2) {
            addCriterion("id_bonus not between", value1, value2, "idBonus");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIsNull() {
            addCriterion("nik_karyawan is null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIsNotNull() {
            addCriterion("nik_karyawan is not null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanEqualTo(String value) {
            addCriterion("nik_karyawan =", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotEqualTo(String value) {
            addCriterion("nik_karyawan <>", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThan(String value) {
            addCriterion("nik_karyawan >", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThanOrEqualTo(String value) {
            addCriterion("nik_karyawan >=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThan(String value) {
            addCriterion("nik_karyawan <", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThanOrEqualTo(String value) {
            addCriterion("nik_karyawan <=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLike(String value) {
            addCriterion("nik_karyawan like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotLike(String value) {
            addCriterion("nik_karyawan not like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIn(List<String> values) {
            addCriterion("nik_karyawan in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotIn(List<String> values) {
            addCriterion("nik_karyawan not in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanBetween(String value1, String value2) {
            addCriterion("nik_karyawan between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotBetween(String value1, String value2) {
            addCriterion("nik_karyawan not between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanIsNull() {
            addCriterion("bonus_karyawan is null");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanIsNotNull() {
            addCriterion("bonus_karyawan is not null");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanEqualTo(BigDecimal value) {
            addCriterion("bonus_karyawan =", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanNotEqualTo(BigDecimal value) {
            addCriterion("bonus_karyawan <>", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanGreaterThan(BigDecimal value) {
            addCriterion("bonus_karyawan >", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bonus_karyawan >=", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanLessThan(BigDecimal value) {
            addCriterion("bonus_karyawan <", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bonus_karyawan <=", value, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanIn(List<BigDecimal> values) {
            addCriterion("bonus_karyawan in", values, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanNotIn(List<BigDecimal> values) {
            addCriterion("bonus_karyawan not in", values, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bonus_karyawan between", value1, value2, "bonusKaryawan");
            return (Criteria) this;
        }

        public Criteria andBonusKaryawanNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bonus_karyawan not between", value1, value2, "bonusKaryawan");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}