package absensi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AbsensiExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AbsensiExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdAbsensiIsNull() {
            addCriterion("id_absensi is null");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiIsNotNull() {
            addCriterion("id_absensi is not null");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiEqualTo(Integer value) {
            addCriterion("id_absensi =", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiNotEqualTo(Integer value) {
            addCriterion("id_absensi <>", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiGreaterThan(Integer value) {
            addCriterion("id_absensi >", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_absensi >=", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiLessThan(Integer value) {
            addCriterion("id_absensi <", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiLessThanOrEqualTo(Integer value) {
            addCriterion("id_absensi <=", value, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiIn(List<Integer> values) {
            addCriterion("id_absensi in", values, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiNotIn(List<Integer> values) {
            addCriterion("id_absensi not in", values, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiBetween(Integer value1, Integer value2) {
            addCriterion("id_absensi between", value1, value2, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andIdAbsensiNotBetween(Integer value1, Integer value2) {
            addCriterion("id_absensi not between", value1, value2, "idAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiIsNull() {
            addCriterion("tanggal_absensi is null");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiIsNotNull() {
            addCriterion("tanggal_absensi is not null");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_absensi =", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiNotEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_absensi <>", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiGreaterThan(Date value) {
            addCriterionForJDBCDate("tanggal_absensi >", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_absensi >=", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiLessThan(Date value) {
            addCriterionForJDBCDate("tanggal_absensi <", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_absensi <=", value, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_absensi in", values, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiNotIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_absensi not in", values, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_absensi between", value1, value2, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andTanggalAbsensiNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_absensi not between", value1, value2, "tanggalAbsensi");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIsNull() {
            addCriterion("nik_karyawan is null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIsNotNull() {
            addCriterion("nik_karyawan is not null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanEqualTo(String value) {
            addCriterion("nik_karyawan =", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotEqualTo(String value) {
            addCriterion("nik_karyawan <>", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThan(String value) {
            addCriterion("nik_karyawan >", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThanOrEqualTo(String value) {
            addCriterion("nik_karyawan >=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThan(String value) {
            addCriterion("nik_karyawan <", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThanOrEqualTo(String value) {
            addCriterion("nik_karyawan <=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLike(String value) {
            addCriterion("nik_karyawan like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotLike(String value) {
            addCriterion("nik_karyawan not like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIn(List<String> values) {
            addCriterion("nik_karyawan in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotIn(List<String> values) {
            addCriterion("nik_karyawan not in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanBetween(String value1, String value2) {
            addCriterion("nik_karyawan between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotBetween(String value1, String value2) {
            addCriterion("nik_karyawan not between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andHadirIsNull() {
            addCriterion("hadir is null");
            return (Criteria) this;
        }

        public Criteria andHadirIsNotNull() {
            addCriterion("hadir is not null");
            return (Criteria) this;
        }

        public Criteria andHadirEqualTo(Integer value) {
            addCriterion("hadir =", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirNotEqualTo(Integer value) {
            addCriterion("hadir <>", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirGreaterThan(Integer value) {
            addCriterion("hadir >", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirGreaterThanOrEqualTo(Integer value) {
            addCriterion("hadir >=", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirLessThan(Integer value) {
            addCriterion("hadir <", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirLessThanOrEqualTo(Integer value) {
            addCriterion("hadir <=", value, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirIn(List<Integer> values) {
            addCriterion("hadir in", values, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirNotIn(List<Integer> values) {
            addCriterion("hadir not in", values, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirBetween(Integer value1, Integer value2) {
            addCriterion("hadir between", value1, value2, "hadir");
            return (Criteria) this;
        }

        public Criteria andHadirNotBetween(Integer value1, Integer value2) {
            addCriterion("hadir not between", value1, value2, "hadir");
            return (Criteria) this;
        }

        public Criteria andAlphaIsNull() {
            addCriterion("alpha is null");
            return (Criteria) this;
        }

        public Criteria andAlphaIsNotNull() {
            addCriterion("alpha is not null");
            return (Criteria) this;
        }

        public Criteria andAlphaEqualTo(Integer value) {
            addCriterion("alpha =", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaNotEqualTo(Integer value) {
            addCriterion("alpha <>", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaGreaterThan(Integer value) {
            addCriterion("alpha >", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaGreaterThanOrEqualTo(Integer value) {
            addCriterion("alpha >=", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaLessThan(Integer value) {
            addCriterion("alpha <", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaLessThanOrEqualTo(Integer value) {
            addCriterion("alpha <=", value, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaIn(List<Integer> values) {
            addCriterion("alpha in", values, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaNotIn(List<Integer> values) {
            addCriterion("alpha not in", values, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaBetween(Integer value1, Integer value2) {
            addCriterion("alpha between", value1, value2, "alpha");
            return (Criteria) this;
        }

        public Criteria andAlphaNotBetween(Integer value1, Integer value2) {
            addCriterion("alpha not between", value1, value2, "alpha");
            return (Criteria) this;
        }

        public Criteria andIzinIsNull() {
            addCriterion("izin is null");
            return (Criteria) this;
        }

        public Criteria andIzinIsNotNull() {
            addCriterion("izin is not null");
            return (Criteria) this;
        }

        public Criteria andIzinEqualTo(Integer value) {
            addCriterion("izin =", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinNotEqualTo(Integer value) {
            addCriterion("izin <>", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinGreaterThan(Integer value) {
            addCriterion("izin >", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinGreaterThanOrEqualTo(Integer value) {
            addCriterion("izin >=", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinLessThan(Integer value) {
            addCriterion("izin <", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinLessThanOrEqualTo(Integer value) {
            addCriterion("izin <=", value, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinIn(List<Integer> values) {
            addCriterion("izin in", values, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinNotIn(List<Integer> values) {
            addCriterion("izin not in", values, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinBetween(Integer value1, Integer value2) {
            addCriterion("izin between", value1, value2, "izin");
            return (Criteria) this;
        }

        public Criteria andIzinNotBetween(Integer value1, Integer value2) {
            addCriterion("izin not between", value1, value2, "izin");
            return (Criteria) this;
        }

        public Criteria andSakitIsNull() {
            addCriterion("sakit is null");
            return (Criteria) this;
        }

        public Criteria andSakitIsNotNull() {
            addCriterion("sakit is not null");
            return (Criteria) this;
        }

        public Criteria andSakitEqualTo(Integer value) {
            addCriterion("sakit =", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitNotEqualTo(Integer value) {
            addCriterion("sakit <>", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitGreaterThan(Integer value) {
            addCriterion("sakit >", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitGreaterThanOrEqualTo(Integer value) {
            addCriterion("sakit >=", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitLessThan(Integer value) {
            addCriterion("sakit <", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitLessThanOrEqualTo(Integer value) {
            addCriterion("sakit <=", value, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitIn(List<Integer> values) {
            addCriterion("sakit in", values, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitNotIn(List<Integer> values) {
            addCriterion("sakit not in", values, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitBetween(Integer value1, Integer value2) {
            addCriterion("sakit between", value1, value2, "sakit");
            return (Criteria) this;
        }

        public Criteria andSakitNotBetween(Integer value1, Integer value2) {
            addCriterion("sakit not between", value1, value2, "sakit");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}