package absensi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MasterKaryawanExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MasterKaryawanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andNikKaryawanIsNull() {
            addCriterion("nik_karyawan is null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIsNotNull() {
            addCriterion("nik_karyawan is not null");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanEqualTo(String value) {
            addCriterion("nik_karyawan =", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotEqualTo(String value) {
            addCriterion("nik_karyawan <>", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThan(String value) {
            addCriterion("nik_karyawan >", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanGreaterThanOrEqualTo(String value) {
            addCriterion("nik_karyawan >=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThan(String value) {
            addCriterion("nik_karyawan <", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLessThanOrEqualTo(String value) {
            addCriterion("nik_karyawan <=", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanLike(String value) {
            addCriterion("nik_karyawan like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotLike(String value) {
            addCriterion("nik_karyawan not like", value, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanIn(List<String> values) {
            addCriterion("nik_karyawan in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotIn(List<String> values) {
            addCriterion("nik_karyawan not in", values, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanBetween(String value1, String value2) {
            addCriterion("nik_karyawan between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNikKaryawanNotBetween(String value1, String value2) {
            addCriterion("nik_karyawan not between", value1, value2, "nikKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanIsNull() {
            addCriterion("nama_karyawan is null");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanIsNotNull() {
            addCriterion("nama_karyawan is not null");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanEqualTo(String value) {
            addCriterion("nama_karyawan =", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanNotEqualTo(String value) {
            addCriterion("nama_karyawan <>", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanGreaterThan(String value) {
            addCriterion("nama_karyawan >", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanGreaterThanOrEqualTo(String value) {
            addCriterion("nama_karyawan >=", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanLessThan(String value) {
            addCriterion("nama_karyawan <", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanLessThanOrEqualTo(String value) {
            addCriterion("nama_karyawan <=", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanLike(String value) {
            addCriterion("nama_karyawan like", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanNotLike(String value) {
            addCriterion("nama_karyawan not like", value, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanIn(List<String> values) {
            addCriterion("nama_karyawan in", values, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanNotIn(List<String> values) {
            addCriterion("nama_karyawan not in", values, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanBetween(String value1, String value2) {
            addCriterion("nama_karyawan between", value1, value2, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andNamaKaryawanNotBetween(String value1, String value2) {
            addCriterion("nama_karyawan not between", value1, value2, "namaKaryawan");
            return (Criteria) this;
        }

        public Criteria andTempatLahirIsNull() {
            addCriterion("tempat_lahir is null");
            return (Criteria) this;
        }

        public Criteria andTempatLahirIsNotNull() {
            addCriterion("tempat_lahir is not null");
            return (Criteria) this;
        }

        public Criteria andTempatLahirEqualTo(String value) {
            addCriterion("tempat_lahir =", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirNotEqualTo(String value) {
            addCriterion("tempat_lahir <>", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirGreaterThan(String value) {
            addCriterion("tempat_lahir >", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirGreaterThanOrEqualTo(String value) {
            addCriterion("tempat_lahir >=", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirLessThan(String value) {
            addCriterion("tempat_lahir <", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirLessThanOrEqualTo(String value) {
            addCriterion("tempat_lahir <=", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirLike(String value) {
            addCriterion("tempat_lahir like", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirNotLike(String value) {
            addCriterion("tempat_lahir not like", value, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirIn(List<String> values) {
            addCriterion("tempat_lahir in", values, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirNotIn(List<String> values) {
            addCriterion("tempat_lahir not in", values, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirBetween(String value1, String value2) {
            addCriterion("tempat_lahir between", value1, value2, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTempatLahirNotBetween(String value1, String value2) {
            addCriterion("tempat_lahir not between", value1, value2, "tempatLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirIsNull() {
            addCriterion("tanggal_lahir is null");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirIsNotNull() {
            addCriterion("tanggal_lahir is not null");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_lahir =", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirNotEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_lahir <>", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirGreaterThan(Date value) {
            addCriterionForJDBCDate("tanggal_lahir >", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_lahir >=", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirLessThan(Date value) {
            addCriterionForJDBCDate("tanggal_lahir <", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_lahir <=", value, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_lahir in", values, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirNotIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_lahir not in", values, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_lahir between", value1, value2, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andTanggalLahirNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_lahir not between", value1, value2, "tanggalLahir");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminIsNull() {
            addCriterion("jenis_kelamin is null");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminIsNotNull() {
            addCriterion("jenis_kelamin is not null");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminEqualTo(String value) {
            addCriterion("jenis_kelamin =", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminNotEqualTo(String value) {
            addCriterion("jenis_kelamin <>", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminGreaterThan(String value) {
            addCriterion("jenis_kelamin >", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminGreaterThanOrEqualTo(String value) {
            addCriterion("jenis_kelamin >=", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminLessThan(String value) {
            addCriterion("jenis_kelamin <", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminLessThanOrEqualTo(String value) {
            addCriterion("jenis_kelamin <=", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminLike(String value) {
            addCriterion("jenis_kelamin like", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminNotLike(String value) {
            addCriterion("jenis_kelamin not like", value, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminIn(List<String> values) {
            addCriterion("jenis_kelamin in", values, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminNotIn(List<String> values) {
            addCriterion("jenis_kelamin not in", values, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminBetween(String value1, String value2) {
            addCriterion("jenis_kelamin between", value1, value2, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andJenisKelaminNotBetween(String value1, String value2) {
            addCriterion("jenis_kelamin not between", value1, value2, "jenisKelamin");
            return (Criteria) this;
        }

        public Criteria andAgamaIsNull() {
            addCriterion("agama is null");
            return (Criteria) this;
        }

        public Criteria andAgamaIsNotNull() {
            addCriterion("agama is not null");
            return (Criteria) this;
        }

        public Criteria andAgamaEqualTo(String value) {
            addCriterion("agama =", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaNotEqualTo(String value) {
            addCriterion("agama <>", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaGreaterThan(String value) {
            addCriterion("agama >", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaGreaterThanOrEqualTo(String value) {
            addCriterion("agama >=", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaLessThan(String value) {
            addCriterion("agama <", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaLessThanOrEqualTo(String value) {
            addCriterion("agama <=", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaLike(String value) {
            addCriterion("agama like", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaNotLike(String value) {
            addCriterion("agama not like", value, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaIn(List<String> values) {
            addCriterion("agama in", values, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaNotIn(List<String> values) {
            addCriterion("agama not in", values, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaBetween(String value1, String value2) {
            addCriterion("agama between", value1, value2, "agama");
            return (Criteria) this;
        }

        public Criteria andAgamaNotBetween(String value1, String value2) {
            addCriterion("agama not between", value1, value2, "agama");
            return (Criteria) this;
        }

        public Criteria andKontakIsNull() {
            addCriterion("kontak is null");
            return (Criteria) this;
        }

        public Criteria andKontakIsNotNull() {
            addCriterion("kontak is not null");
            return (Criteria) this;
        }

        public Criteria andKontakEqualTo(String value) {
            addCriterion("kontak =", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakNotEqualTo(String value) {
            addCriterion("kontak <>", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakGreaterThan(String value) {
            addCriterion("kontak >", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakGreaterThanOrEqualTo(String value) {
            addCriterion("kontak >=", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakLessThan(String value) {
            addCriterion("kontak <", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakLessThanOrEqualTo(String value) {
            addCriterion("kontak <=", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakLike(String value) {
            addCriterion("kontak like", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakNotLike(String value) {
            addCriterion("kontak not like", value, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakIn(List<String> values) {
            addCriterion("kontak in", values, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakNotIn(List<String> values) {
            addCriterion("kontak not in", values, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakBetween(String value1, String value2) {
            addCriterion("kontak between", value1, value2, "kontak");
            return (Criteria) this;
        }

        public Criteria andKontakNotBetween(String value1, String value2) {
            addCriterion("kontak not between", value1, value2, "kontak");
            return (Criteria) this;
        }

        public Criteria andDivisiIsNull() {
            addCriterion("divisi is null");
            return (Criteria) this;
        }

        public Criteria andDivisiIsNotNull() {
            addCriterion("divisi is not null");
            return (Criteria) this;
        }

        public Criteria andDivisiEqualTo(String value) {
            addCriterion("divisi =", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiNotEqualTo(String value) {
            addCriterion("divisi <>", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiGreaterThan(String value) {
            addCriterion("divisi >", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiGreaterThanOrEqualTo(String value) {
            addCriterion("divisi >=", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiLessThan(String value) {
            addCriterion("divisi <", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiLessThanOrEqualTo(String value) {
            addCriterion("divisi <=", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiLike(String value) {
            addCriterion("divisi like", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiNotLike(String value) {
            addCriterion("divisi not like", value, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiIn(List<String> values) {
            addCriterion("divisi in", values, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiNotIn(List<String> values) {
            addCriterion("divisi not in", values, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiBetween(String value1, String value2) {
            addCriterion("divisi between", value1, value2, "divisi");
            return (Criteria) this;
        }

        public Criteria andDivisiNotBetween(String value1, String value2) {
            addCriterion("divisi not between", value1, value2, "divisi");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaIsNull() {
            addCriterion("masa_kerja is null");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaIsNotNull() {
            addCriterion("masa_kerja is not null");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaEqualTo(String value) {
            addCriterion("masa_kerja =", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaNotEqualTo(String value) {
            addCriterion("masa_kerja <>", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaGreaterThan(String value) {
            addCriterion("masa_kerja >", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaGreaterThanOrEqualTo(String value) {
            addCriterion("masa_kerja >=", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaLessThan(String value) {
            addCriterion("masa_kerja <", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaLessThanOrEqualTo(String value) {
            addCriterion("masa_kerja <=", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaLike(String value) {
            addCriterion("masa_kerja like", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaNotLike(String value) {
            addCriterion("masa_kerja not like", value, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaIn(List<String> values) {
            addCriterion("masa_kerja in", values, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaNotIn(List<String> values) {
            addCriterion("masa_kerja not in", values, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaBetween(String value1, String value2) {
            addCriterion("masa_kerja between", value1, value2, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andMasaKerjaNotBetween(String value1, String value2) {
            addCriterion("masa_kerja not between", value1, value2, "masaKerja");
            return (Criteria) this;
        }

        public Criteria andIdJabatanIsNull() {
            addCriterion("id_jabatan is null");
            return (Criteria) this;
        }

        public Criteria andIdJabatanIsNotNull() {
            addCriterion("id_jabatan is not null");
            return (Criteria) this;
        }

        public Criteria andIdJabatanEqualTo(Integer value) {
            addCriterion("id_jabatan =", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotEqualTo(Integer value) {
            addCriterion("id_jabatan <>", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanGreaterThan(Integer value) {
            addCriterion("id_jabatan >", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_jabatan >=", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanLessThan(Integer value) {
            addCriterion("id_jabatan <", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanLessThanOrEqualTo(Integer value) {
            addCriterion("id_jabatan <=", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanIn(List<Integer> values) {
            addCriterion("id_jabatan in", values, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotIn(List<Integer> values) {
            addCriterion("id_jabatan not in", values, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanBetween(Integer value1, Integer value2) {
            addCriterion("id_jabatan between", value1, value2, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotBetween(Integer value1, Integer value2) {
            addCriterion("id_jabatan not between", value1, value2, "idJabatan");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}