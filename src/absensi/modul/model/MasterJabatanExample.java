package absensi.modul.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MasterJabatanExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MasterJabatanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdJabatanIsNull() {
            addCriterion("id_jabatan is null");
            return (Criteria) this;
        }

        public Criteria andIdJabatanIsNotNull() {
            addCriterion("id_jabatan is not null");
            return (Criteria) this;
        }

        public Criteria andIdJabatanEqualTo(Integer value) {
            addCriterion("id_jabatan =", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotEqualTo(Integer value) {
            addCriterion("id_jabatan <>", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanGreaterThan(Integer value) {
            addCriterion("id_jabatan >", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_jabatan >=", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanLessThan(Integer value) {
            addCriterion("id_jabatan <", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanLessThanOrEqualTo(Integer value) {
            addCriterion("id_jabatan <=", value, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanIn(List<Integer> values) {
            addCriterion("id_jabatan in", values, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotIn(List<Integer> values) {
            addCriterion("id_jabatan not in", values, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanBetween(Integer value1, Integer value2) {
            addCriterion("id_jabatan between", value1, value2, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andIdJabatanNotBetween(Integer value1, Integer value2) {
            addCriterion("id_jabatan not between", value1, value2, "idJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanIsNull() {
            addCriterion("nama_jabatan is null");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanIsNotNull() {
            addCriterion("nama_jabatan is not null");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanEqualTo(String value) {
            addCriterion("nama_jabatan =", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanNotEqualTo(String value) {
            addCriterion("nama_jabatan <>", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanGreaterThan(String value) {
            addCriterion("nama_jabatan >", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanGreaterThanOrEqualTo(String value) {
            addCriterion("nama_jabatan >=", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanLessThan(String value) {
            addCriterion("nama_jabatan <", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanLessThanOrEqualTo(String value) {
            addCriterion("nama_jabatan <=", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanLike(String value) {
            addCriterion("nama_jabatan like", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanNotLike(String value) {
            addCriterion("nama_jabatan not like", value, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanIn(List<String> values) {
            addCriterion("nama_jabatan in", values, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanNotIn(List<String> values) {
            addCriterion("nama_jabatan not in", values, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanBetween(String value1, String value2) {
            addCriterion("nama_jabatan between", value1, value2, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andNamaJabatanNotBetween(String value1, String value2) {
            addCriterion("nama_jabatan not between", value1, value2, "namaJabatan");
            return (Criteria) this;
        }

        public Criteria andGajiIsNull() {
            addCriterion("gaji is null");
            return (Criteria) this;
        }

        public Criteria andGajiIsNotNull() {
            addCriterion("gaji is not null");
            return (Criteria) this;
        }

        public Criteria andGajiEqualTo(BigDecimal value) {
            addCriterion("gaji =", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiNotEqualTo(BigDecimal value) {
            addCriterion("gaji <>", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiGreaterThan(BigDecimal value) {
            addCriterion("gaji >", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("gaji >=", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiLessThan(BigDecimal value) {
            addCriterion("gaji <", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiLessThanOrEqualTo(BigDecimal value) {
            addCriterion("gaji <=", value, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiIn(List<BigDecimal> values) {
            addCriterion("gaji in", values, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiNotIn(List<BigDecimal> values) {
            addCriterion("gaji not in", values, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gaji between", value1, value2, "gaji");
            return (Criteria) this;
        }

        public Criteria andGajiNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gaji not between", value1, value2, "gaji");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}