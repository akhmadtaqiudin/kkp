package absensi.modul.model;

import java.math.BigDecimal;

public class Bonus {
    private Integer idBonus;

    private String nikKaryawan;

    private String namaKaryawan;

    private BigDecimal bonusKaryawan;

    private String masaKerja;
    
    private String namaJabatan;

    private BigDecimal gaji;

    public Integer getIdBonus() {
        return idBonus;
    }

    public void setIdBonus(Integer idBonus) {
        this.idBonus = idBonus;
    }

    public String getNikKaryawan() {
        return nikKaryawan;
    }

    public void setNikKaryawan(String nikKaryawan) {
        this.nikKaryawan = nikKaryawan == null ? null : nikKaryawan.trim();
    }

    public BigDecimal getBonusKaryawan() {
        return bonusKaryawan;
    }

    public void setBonusKaryawan(BigDecimal bonusKaryawan) {
        this.bonusKaryawan = bonusKaryawan;
    }

	public String getNamaKaryawan() {
		return namaKaryawan;
	}

	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}

	public String getMasaKerja() {
		return masaKerja;
	}

	public void setMasaKerja(String masaKerja) {
		this.masaKerja = masaKerja;
	}

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	public BigDecimal getGaji() {
		return gaji;
	}

	public void setGaji(BigDecimal gaji) {
		this.gaji = gaji;
	}
}