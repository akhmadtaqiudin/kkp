package absensi.modul.model;

import java.math.BigDecimal;
import java.util.Date;

public class MasterKaryawan {
    private String nikKaryawan;

    private String namaKaryawan;

    private String tempatLahir;

    private Date tanggalLahir;

    private String jenisKelamin;

    private String agama;

    private String kontak;

    private String divisi;

    private String masaKerja;

    private Integer idJabatan;
    
    private String namaJabatan;

    private BigDecimal gaji;

    public String getNikKaryawan() {
        return nikKaryawan;
    }

    public void setNikKaryawan(String nikKaryawan) {
        this.nikKaryawan = nikKaryawan == null ? null : nikKaryawan.trim();
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan == null ? null : namaKaryawan.trim();
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir == null ? null : tempatLahir.trim();
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin == null ? null : jenisKelamin.trim();
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama == null ? null : agama.trim();
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak == null ? null : kontak.trim();
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi == null ? null : divisi.trim();
    }

    public String getMasaKerja() {
        return masaKerja;
    }

    public void setMasaKerja(String masaKerja) {
        this.masaKerja = masaKerja == null ? null : masaKerja.trim();
    }

    public Integer getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(Integer idJabatan) {
        this.idJabatan = idJabatan;
    }

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	public BigDecimal getGaji() {
		return gaji;
	}

	public void setGaji(BigDecimal gaji) {
		this.gaji = gaji;
	}
}