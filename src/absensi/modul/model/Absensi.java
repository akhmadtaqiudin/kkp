package absensi.modul.model;

import java.util.Date;

public class Absensi {
    private Integer idAbsensi;

    private Date tanggalAbsensi;

    private String nikKaryawan;

    private String namaKaryawan;

    private Integer hadir;

    private Integer alpha;

    private Integer izin;

    private Integer sakit;

    public Integer getIdAbsensi() {
        return idAbsensi;
    }

    public void setIdAbsensi(Integer idAbsensi) {
        this.idAbsensi = idAbsensi;
    }

    public Date getTanggalAbsensi() {
        return tanggalAbsensi;
    }

    public void setTanggalAbsensi(Date tanggalAbsensi) {
        this.tanggalAbsensi = tanggalAbsensi;
    }

    public String getNikKaryawan() {
        return nikKaryawan;
    }

    public void setNikKaryawan(String nikKaryawan) {
        this.nikKaryawan = nikKaryawan == null ? null : nikKaryawan.trim();
    }

    public Integer getHadir() {
        return hadir;
    }

    public void setHadir(Integer hadir) {
        this.hadir = hadir;
    }

    public Integer getAlpha() {
        return alpha;
    }

    public void setAlpha(Integer alpha) {
        this.alpha = alpha;
    }

    public Integer getIzin() {
        return izin;
    }

    public void setIzin(Integer izin) {
        this.izin = izin;
    }

    public Integer getSakit() {
        return sakit;
    }

    public void setSakit(Integer sakit) {
        this.sakit = sakit;
    }

	public String getNamaKaryawan() {
		return namaKaryawan;
	}

	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
}