package absensi.modul.model;

import java.math.BigDecimal;

public class MasterJabatan {
    private Integer idJabatan;

    private String namaJabatan;

    private BigDecimal gaji;

    public Integer getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(Integer idJabatan) {
        this.idJabatan = idJabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan == null ? null : namaJabatan.trim();
    }

    public BigDecimal getGaji() {
        return gaji;
    }

    public void setGaji(BigDecimal gaji) {
        this.gaji = gaji;
    }
}