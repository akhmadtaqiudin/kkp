package absensi.core.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import absensi.core.mapper.MasterUserMapper;
import absensi.core.model.MasterUser;
import absensi.core.model.MasterUserExample;

public class MasterUserAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private MasterUser user;
	private List<MasterUser> listUser = new ArrayList<>();
	private Map<String, Object> session;
	private MasterUserMapper masterUserMapper = (MasterUserMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("masterUserMapper");
	
	public String Login(){
		System.out.println("Jalankan Method Login");
		
		MasterUserExample ex = new MasterUserExample();
		ex.createCriteria().andUserNameEqualTo(user.getUserName()).andPasswordEqualTo(user.getPassword()).andHakAksesEqualTo(user.getHakAkses());
		
		long x=0;
		x = masterUserMapper.countByExample(ex);
		
		if(x>0){
			session.put("userName",user);
			if(user.getHakAkses().equalsIgnoreCase("admin") && session != null){
				user =(MasterUser) session.get("userName");
				return "admin";
			}else if(user.getHakAkses().equalsIgnoreCase("manager") && session != null){
				user =(MasterUser) session.get("userName");
				return "manager";
			}else{
				return ERROR;
			}
		}else{
			addFieldError("invalid", "Username & Password Salah");
		}
		return ERROR;
	}
	
	public String LogOut(){
		System.out.println("Jalankan method LogOut");
		session.remove("userName");
		return SUCCESS;
	}
	public MasterUser getUser() {
		return user;
	}
	public void setUser(MasterUser user) {
		this.user = user;
	}
	public List<MasterUser> getListUser() {
		return listUser;
	}
	public void setListUser(List<MasterUser> listUser) {
		this.listUser = listUser;
	}
	public MasterUserMapper getMasterUserMapper() {
		return masterUserMapper;
	}
	public void setMasterUserMapper(MasterUserMapper masterUserMapper) {
		this.masterUserMapper = masterUserMapper;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
