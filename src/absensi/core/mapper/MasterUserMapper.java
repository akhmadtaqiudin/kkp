package absensi.core.mapper;

import absensi.core.model.MasterUser;
import absensi.core.model.MasterUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MasterUserMapper {
    long countByExample(MasterUserExample example);

    int deleteByExample(MasterUserExample example);

    int deleteByPrimaryKey(String userName);

    int insert(MasterUser record);

    int insertSelective(MasterUser record);

    List<MasterUser> selectByExample(MasterUserExample example);

    MasterUser selectByPrimaryKey(String userName);

    int updateByExampleSelective(@Param("record") MasterUser record, @Param("example") MasterUserExample example);

    int updateByExample(@Param("record") MasterUser record, @Param("example") MasterUserExample example);

    int updateByPrimaryKeySelective(MasterUser record);

    int updateByPrimaryKey(MasterUser record);
}