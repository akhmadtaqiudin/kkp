<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style type="text/css">
    	/*scroll effect*/
		.navbar-trans {
		  background-color: transparent;
		  border: none;
		  transition: top 1s ease;
		}
		 
		/*double row*/
		.navbar-doublerow > .navbar{
			display: block;	
			padding: 0px auto;
			margin: 0px auto;
			min-height: 25px;
		}
		.navbar-doublerow .nav{
			padding: 0px auto;
		}
		.navbar-doublerow .dividline{
		  margin: 5px 100px;
		  padding-top: 1px;
		  background-color: inherit;
		}
		/*top nav*/
		.navbar-doublerow .navbar-top ul>li>a {
			padding: 10px auto;
			font-size: 12px;
		} 
		/*down nav*/
		.navbar-doublerow .navbar-down .navbar-brand {
			padding: 0px auto;
			float: left;
			color: #fff;
			font-size: 32px;
		}
		.navbar-doublerow .navbar-down ul>li>a{
			font-size: 16px;
			color: #fff;
			transition: border-bottom .2s ease-in , transform .2s ease-in-out;
		}
		.navbar-doublerow .navbar-down ul>li>a:hover{
			border-bottom: 1px solid #fff;
			color: #fff;
		}
		.navbar-doublerow .navbar-down .dropdown{
		    padding: 5px;
		    color: #000;
		}
		.navbar-doublerow .navbar-down .dropdown ul>li>a,
		.navbar-doublerow .navbar-down .dropdown ul>li>a:hover{
		  color: #000;
		  border-bottom: none;
		}
		.navbar-doublerow.navbar-trans.afterscroll {
		}	
		.navbar-doublerow.navbar-trans.afterscroll {
		   top:-50px;
		}	
		
		.flex-container {
		    display: flex;
		    justify-content: space-between;
		}
		.flex-item {
		}
		/*text*/
		.text-white,.text-white-hover:hover{color:#fff!important}
		/*fontcolor*/
		.light-grey {color:#000!important;background-color:#E6E9ED!important}
		span{
			font-size: 16px;
		}
		.jumbotron{margin-top: 100px;opacity: 0.9;height: 550px;}
		h3{text-align: center;color:#000!important}
		hr{height: 7px;background-color: #000!important}
    </style>
	<title>Jasindo</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-doublerow navbar-trans navbar-fixed-top">
  <!-- top nav -->
  <nav class="navbar navbar-top hidden-xs">
    <div class="container">
      <!-- left nav top -->
      <ul class="nav navbar-nav pull-left">
        <!-- <li><a href="#"><span class="glyphicon glyphicon-thumbs-up text-white"></span></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-globe text-white"></span></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-pushpin text-white"></span></a></li> -->
        <li><a href="#"><span class="text-white">QUESTIONS? CALL: <b>+963000000000</b></span></a></li>
      </ul>
      <!-- right nav top -->
      <ul class="nav navbar-nav pull-right">
        <li><a href="https://github.com/alphadsy" class="text-white">Graha MR-21, Lantai 10 Jl. Menteng Raya No. 21 Jakarta Pusat 10340</a></li>
       <!--  <li><a href="https://github.com/alphadsy" class="text-white">Contact Us</a></li>  -->
      </ul>
    </div>
    <div class="dividline light-grey"></div>
  </nav>
  <!-- down nav -->
  <nav class="navbar navbar-down">
    <div class="container">
      <div class="flex-container">  
        <div class="navbar-header flex-item">
          <div class="navbar-brand">JASINDO<span>.co.id</span></div>
        </div>
        <ul class="nav navbar-nav flex-item hidden-xs">
          <li><a href="/Absensi/">Home</a></li>
          <li><a href="about.jsp">About</a></li> 
          <li><a href="gallery.jsp">Gallery</a></li>
        </ul>
        <ul class="nav navbar-nav flex-item hidden-xs pull-right">
          <li><a href="${pageContext.request.contextPath}/login.action" class="">Login</a></li> 
        </ul>
        <!-- dropdown only moblie -->
          <div class="dropdown visible-xs pull-right">
            <button class="btn btn-default dropdown-toggle " type="button" id="dropdownmenu" data-toggle="dropdown">
              <span class="glyphicon glyphicon-align-justify"></span> 
            </button>
            <ul class="dropdown-menu">
              <li><a href="/Absensi/">Home</a></li>
	          <li><a href="about.jsp">About</a></li> 
	          <li><a href="gallery.jsp">Gallery</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="${pageContext.request.contextPath}/login.action">Login</a></li>
            </ul>
          </div>
        </div>  
          <div class="jumbotron">
          		<div class="box-title">
          			<h3>IDENTITAS PERUSAHAAN</h3>
          			<hr>
          		</div>
          		<div class="row">
          			<div class="col-md-6">
          				<ul>
          					<li style="margin-bottom: 10px; color:#000!important">Nama Perusahaan / Corporate Brand <br><span><b>PT Asuransi Jasindo Syariah</b></span></li>
          					<li style="margin-bottom: 10px; color:#000!important">Tanggal Pendirian / Date of Established <br><span><b>27 Januari 2016</b></span></li>
          					<li style="margin-bottom: 10px; color:#000!important">Tanggal Beroperasi / Operating Since <br><span><b>02 Mei 2016</b></span></li>
          					<li style="margin-bottom: 10px; color:#000!important">Bidang Usaha / Type of Business <br><span><b>Asuransi Umum dengan Prinsip Syariah</b></span></li>
          				</ul>
          			</div>
          			<div class="col-md-6">
          				<ul> 
          					<li style="margin-bottom: 10px; color:#000!important">Lini Usaha / Line of Business <br><span><b>Delapan lini usaha asuransi umum dengan menggunakan prinsip syariah</b></span></li>
          					<li style="margin-bottom: 10px; color:#000!important">Kepemilikan / Ownership <br><span>PT Asuransi Jasa Indonesia (Persero) : <b>96,5%</b></span><br><span>Yayasan Kesejahteraan Karyawan Jasindo : <b>3,5%</b></span></li>
          				</ul>
          			</div>
          		</div>
          </div>
      </div>
    </nav>
  </nav>  
<header>
    <img src="${pageContext.request.contextPath}/asset/images/slider2.jpg" style="width:100%; height: 850px;">
</header>
	<script type="text/javascript">
		// toggle class scroll 
		$(window).scroll(function() {
		    if($(this).scrollTop() > 50)
		    {
		        $('.navbar-trans').addClass('afterscroll');
		    } else
		    {
		        $('.navbar-trans').removeClass('afterscroll');
		    }  
	
		});
		  
		// demo only 
		// open link in new tab without ugly target="_blank"
		$("a[href^='http']").attr("target", "_blank");
	</script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/asset/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/bootstrap.min.js"></script>
</body>
</html>