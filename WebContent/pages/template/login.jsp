<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style type="text/css">
		body{background: #eee !important;}
		.wrapper {margin-top: 80px;}
		.form-signin {
			max-width: 450px;
		    padding: 15px 35px 45px;
		    margin: 0 auto;
		    background-color: #fff;
		    border-top-right-radius: 100px;
		    border-bottom-left-radius: 100px;
		}
  		.form-signin-heading{margin-bottom: 30px;text-align: center;}	
		.btn-block{height: 37px;margin-top: 20px;}
		.form-control {
		  position: relative;
		  font-size: 16px;
		  height: 37px;
		  padding: 10px;
			@include box-sizing(border-box);
	
			&:focus {
			  z-index: 2;
			}
		}
    </style>
	<title>Jasindo</title>
</head>
<body>
	<div class="wrapper">
	    <s:form cssClass="form-signin" namespace="/authentication" action="auth">       
	      <h2 class="form-signin-heading">Please login</h2>
	      <s:textfield name="user.userName" required="true" placeholder="username" cssClass="form-control" />
					<s:password name="user.password" required="true" placeholder="password" cssClass="form-control" />
					<s:select list="#{'admin':'Administrator','manager':'Manager'}" name="user.hakAkses" required="true" cssClass="form-control" />
					<s:submit cssClass="btn btn-primary btn-block" value="Login"/>  
	    </s:form>
	  </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/asset/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/bootstrap.min.js"></script>
</body>
</html>