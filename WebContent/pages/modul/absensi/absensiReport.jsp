<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>    
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@page import="absensi.modul.model.MasterJabatan"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/asset/css/jquery-ui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/asset/css/jquery-ui.theme.min.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/asset/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/bootstrap.min.js"></script>
    <!-- Custom styles for this template -->
    <style type="text/css">
		.sidebar {
		    position: fixed;
		    top: 81px;
		    bottom: 0;
		    left: 0;
		    z-index: 1000;
		    display: block;
		    padding: 20px;
		    overflow-x: hidden;
		    overflow-y: auto;
		    background-color: #f5f5f5;
		    border-right: 1px solid #eee;
		}
		.main {padding: 20px; top: 33px;}
		.navbar-brand{height: 30px; padding: 4px 15px!important}
		.navbar-default{background-color: #08a82b;}
		.dropdown-menu{min-width: 128px;}
		.dropdown-menu>li>a{text-align: center;}
		.navbar-default .navbar-nav>li>a{color: #fff;}
		.box-content{margin-top: 45px;}
		.paren-title{
			background-color: #08a82b;
		    border-radius: 7px;
		    color: #fff;
		    text-align: center;
		    font-size: 16px;
		}
		#tgl1{margin-left: -215px;}
		#tgl2{margin-top: -34px;}
		#TablelistAbsensi>thead>tr>th {text-align: center; vertical-align: middle;background-color: #08a82b;color: #fff}
		#TablelistAbsensi>tbody>tr>td {text-align: center; vertical-align: middle;}
		.btn-src{margin-top: 20px;}
		.btn-reset{margin-top: 53px; margin-right: -62px;}
		.btn-add{margin-top: 55px; margin-right: 5px;}
		.pagination{margin-top: 35px; margin-bottom: 10px;}
    </style>
    <script type="text/javascript">
		$(window).ready(function(){
			$(".btn-reset").on("click",function(){
				$(".tgl").val("");			
			});
			
			$( "#tgl1" ).datepicker({
			    dateFormat: 'dd/mm/yy'
			});
			$( "#tgl2" ).datepicker({
			    dateFormat: 'dd/mm/yy'
			});
		});
	</script>
	<title>Jasindo</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img alt="loading" src="${pageContext.request.contextPath}/asset/images/logo.png" class="img-responsive" style="width: 12%;"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>&nbsp;<s:property value="#session['userName'].userName" />&nbsp;<span class="glyphicon glyphicon-cog"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><img src="${pageContext.request.contextPath}/asset/images/admin.jpg" class="img-circle" alt="User Image" style="width: 36%; margin-left: 36px;" /></li>
                        <li><a href="${pageContext.request.contextPath}/authentication/logout.action"><i class="fa fa-sign-out"></i> Sign out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="#" class="paren-title">Data Master</a></li>
            <li><a href="${pageContext.request.contextPath}/absensi/SearchAbsensi.action">Absensi</a></li>
            <li><a href="${pageContext.request.contextPath}/bonus-karyawan/SearchBonusKaryawan.action">Bonus Karyawan</a></li>
            <li><a href="${pageContext.request.contextPath}/master-jabatan/SearchJabatan.action">Jabatan</a></li>
            <li><a href="${pageContext.request.contextPath}/master-karyawan/SearchKaryawan.action">Karyawan</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="#" class="paren-title">Laporan</a></li>
            <li><a href="${pageContext.request.contextPath}/absensi/ReportAbsensi.action">Laporan Absensi</a></li>
            <li><a href="${pageContext.request.contextPath}/bonus-karyawan/ReportBonusKaryawan.action">Laporan Bonus Karyawan</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="box-content">
			<s:form namespace="/absensi" method="pos" cssStyle="float: right; margin-left: -60px;">
				<s:textfield name="strDateAwal" placeholder="Dari Tanggal" cssClass="form-control tgl" id="tgl1" />
				<s:textfield name="strDateAkhir" placeholder="Sampai Tanggal" cssClass="form-control tgl" id="tgl2" />
				<s:submit value="Search" action="ReportAbsensi" cssClass="btn btn-default btn-src" /> 
				<a href="${pageContext.request.contextPath}/absensi/AddJabatan.action" class="btn btn-success btn-add"><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span>&nbsp;Export Excel</a>
				<input type="button" value="Reset" class="btn btn-default btn-reset" />
			 </s:form>
				<s:if test="%{listAbsensi.isEmpty()}">
					<table class="table table-bordered" style="text-align: center; vertical-align: middle; margin-top: 68px;">
			  			<thead>
			  				<tr>
			  					<td><b>Tanggal Absensi</b></td>
			  					<td><b>Nama Karyawan</b></td>
			  					<td><b>Alpha</b></td>
			  					<td><b>Hadir</b></td>
			  					<td><b>Izin</b></td>
			  					<td><b>Sakit</b></td>
			  				</tr>
			  				<tr><td colspan="6">Data Tidak di Temukan</td></tr>
			  			</thead>
			  		</table>
				</s:if>
				<s:else>
					<display:table id="TablelistAbsensi" name="listAbsensi" pagesize="10"
					 requestURI="/absensi/ReportAbsensi.action" class="table table-hover table-striped table-bordered" >
						<display:column title="Tanggal Absensi " property="tanggalAbsensi" format="{0,date,dd/MM/yyyy}" />
						<display:column title="Nama Karyawan " property="namaKaryawan" />
						<display:column title="Alpha" property="alpha" />
						<display:column title="Hadir" property="hadir" />
						<display:column title="Izin" property="izin" />
						<display:column title="Sakit" property="sakit" />
					</display:table>
				</s:else>
			</div>
        </div>
      </div>
    </div>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>