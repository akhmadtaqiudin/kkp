<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/asset/css/jquery-ui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/asset/css/jquery-ui.theme.min.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/asset/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/asset/js/bootstrap.min.js"></script>

    <!-- Custom styles for this template -->
    <style type="text/css">
		.sidebar {
		    position: fixed;
		    top: 81px;
		    bottom: 0;
		    left: 0;
		    z-index: 1000;
		    display: block;
		    padding: 20px;
		    overflow-x: hidden;
		    overflow-y: auto;
		    background-color: #f5f5f5;
		    border-right: 1px solid #eee;
		}
		.main {padding: 20px; top: 33px;}
		.navbar-brand{height: 30px; padding: 4px 15px!important}
		.navbar-default{background-color: #08a82b;}
		.dropdown-menu{min-width: 128px;}
		.dropdown-menu>li>a{text-align: center;}
		.navbar-default .navbar-nav>li>a{color: #fff;}
		.box-content{margin-top: 45px;}
		.paren-title{
			background-color: #08a82b;
		    border-radius: 7px;
		    color: #fff;
		    text-align: center;
		    font-size: 16px;
		}
    </style>
	<title>Jasindo</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img alt="loading" src="${pageContext.request.contextPath}/asset/images/logo.png" class="img-responsive" style="width: 12%;"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>&nbsp;<s:property value="#session['userName'].userName" />&nbsp;<span class="glyphicon glyphicon-cog"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><img src="${pageContext.request.contextPath}/asset/images/admin.jpg" class="img-circle" alt="User Image" style="width: 36%; margin-left: 36px;" /></li>
                        <li><a href="${pageContext.request.contextPath}/authentication/logout.action"><i class="fa fa-sign-out"></i> Sign out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="#" class="paren-title">Data Master</a></li>
            <li><a href="${pageContext.request.contextPath}/absensi/SearchAbsensi.action">Absensi</a></li>
            <li><a href="${pageContext.request.contextPath}/bonus-karyawan/SearchBonusKaryawan.action">Bonus Karyawan</a></li>
            <li><a href="${pageContext.request.contextPath}/master-jabatan/SearchJabatan.action">Jabatan</a></li>
            <li><a href="${pageContext.request.contextPath}/master-karyawan/SearchKaryawan.action">Karyawan</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="#" class="paren-title">Laporan</a></li>
            <li><a href="${pageContext.request.contextPath}/absensi/ReportAbsensi.action">Laporan Absensi</a></li>
            <li><a href="${pageContext.request.contextPath}/bonus-karyawan/ReportBonusKaryawan.action">Laporan Bonus Karyawan</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="box-content">
          	<s:action namespace="/master-jabatan" name="SelectAll" id="jabat"/>
			  <s:form namespace="/master-karyawan" method="POST" theme="bootstrap" cssClass="form-horizontal">
			  	<div class="form-group">
					<label class="col-sm-2 control-label">NIK Karyawan  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.nikKaryawan" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nama Karyawan  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.namaKaryawan" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Tempat Lahir  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.tempatLahir" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Tanggal Lahir  :</label>
					<div class="col-sm-10">
						<s:textfield name="strTanggal" cssClass="form-control" id="tgl" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Jenis Kelamin  :</label>
					<div class="col-sm-10">
						<div class="radio">
						  <label>
						    <input type="radio" name="karyawan.jenisKelamin" value="laki-laki">
						    laki-laki
						  </label>
						</div>
						<div class="radio" style="margin-left: 80px; margin-top: -27px;">
						  <label>
						    <input type="radio" name="karyawan.jenisKelamin" value="wanita">
						    wanita
						  </label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Agama  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.agama" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Kontak  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.kontak" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Divisi  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.divisi" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Masa Kerja  :</label>
					<div class="col-sm-10">
						<s:textfield name="karyawan.masaKerja" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">ID Jabatan  :</label>
					<div class="col-sm-10">
						<s:textfield id="ij" name="karyawan.idJabatan" cssClass="form-control" readonly="true"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nama Jabatan  :</label>
					<div class="col-sm-10">
						<s:textfield id="gaji" name="karyawan.namaJabatan" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Gaji :</label>
					<div class="col-sm-10">
						<s:textfield id="gaji" name="karyawan.gaji" cssClass="form-control" readonly="true" />
					</div>
				</div>
				<hr>
			  	<div class="footer">
					<s:submit action="SearchKaryawan" value="Close" cssClass="btn btn-default" cssStyle="float: right; margin-left: 5px;"/>
				</div>
			</s:form>
		  </div>
        </div>
      </div>
    </div>
</body>
</html>